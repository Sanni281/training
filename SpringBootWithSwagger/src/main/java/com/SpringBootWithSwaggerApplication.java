package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(scanBasePackages = "com")
@EnableSwagger2
public class SpringBootWithSwaggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithSwaggerApplication.class, args);
	}

	@Bean						// container will call this method when string boot load the application. 
	public Docket api() {
		System.out.println("object created..");
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com")).build();
	}
}
