package com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {

	@GetMapping(value = "say")
	public String sayHello() {
		return "Welcome to Spring Rest API with Swagger";
	}
}
