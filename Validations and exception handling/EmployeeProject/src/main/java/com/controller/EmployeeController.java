package com.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Employee;
import com.service.EmployeeService;

@RestController

public class EmployeeController {
	
@Autowired
EmployeeService employeeService;


@ResponseStatus(HttpStatus.BAD_REQUEST)
@ExceptionHandler(MethodArgumentNotValidException.class)
public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
 
    ex.getBindingResult().getFieldErrors().forEach(error -> 
        errors.put(error.getField(), error.getDefaultMessage()));
     
    return errors;
}

@PostMapping(value="addEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
public String insertEmployee(@Valid @RequestBody Employee emp)
{
	return employeeService.addEmployeeINfo(emp);
}


@GetMapping(value="getEmployee",produces = MediaType.APPLICATION_JSON_VALUE)
public List<Employee> getAllEmployeeInfo()
{
	return employeeService.getAllEmployee();
}
@DeleteMapping(value = "delete/{id}" )
public String deleteData(@PathVariable("id")  int id)
{
	return employeeService.deleteEmployee(id);
}

@PatchMapping(value="bookupdate",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String updateDetails(@RequestBody Employee emp)	
{
return employeeService.updateEmployee(emp);
}

}
