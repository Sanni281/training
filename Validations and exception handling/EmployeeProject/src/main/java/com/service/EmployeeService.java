package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Employee;
import com.repository.EmployeeDao;

@Service
public class EmployeeService {
@Autowired
EmployeeDao employeeDao;
public String addEmployeeINfo(Employee emp)
{
	if(employeeDao.existsById(emp.getId()))
	{
		return "id  not present";
	}
	else
	{
employeeDao.save(emp);
		return " store";
	}
}
public List<Employee> getAllEmployee()
{
	return employeeDao.findAll();
}

public String deleteEmployee(int id)
{
	if(!employeeDao.existsById(id))
	{
		return "id  not present";
	}
	else
	{
	employeeDao.deleteById(id);;
		return "deleted";
	}

}
public String updateEmployee(Employee emp)
{
	if(!employeeDao.existsById(emp.getId()))
	{
		return "data Store";
	}
	else
	{
Employee p=employeeDao.getById(emp.getId());
p.setName(emp.getName());
p.setSalary(emp.getSalary());

  employeeDao.saveAndFlush(p);
		return "updated ";
	}	
}



}
