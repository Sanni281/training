package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.Employee;

@Repository
public interface EmployeeDao extends JpaRepository<Employee, Integer>  {
//	List<Employee> findByNameLike(String Name);
//	List<Employee> findByNameLikeStartsWith(String Name);
//	List<Employee> findByNameLikeEndWith(String Name);
//
//	List<Employee> findByNameLikeContainingIgnoreCase(String Name);
//
//	List<Employee> findByNameNotLike(String Name);
//	List<Employee> findBySalary(Float Salary);

}
