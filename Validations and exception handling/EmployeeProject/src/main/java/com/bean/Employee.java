package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
public class Employee {
@Id
@NotNull(message = "Id should be required")
private int id;
@NotBlank(message = "Name is mandatory")
@Size(min = 3, max = 10, message = " minimum 3 characters required amd maximum 10")
private String name;
@Positive(message = "salary should be positive")
private float salary;

public Employee() {
	super();
	// TODO Auto-generated constructor stub
}
public Employee(int id,@NotBlank(message = "Name is mandatory") String name, float salary) {
	super();
	this.id = id;
	this.name = name;
	this.salary = salary;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public float getSalary() {
	return salary;
}
public void setSalary(float salary) {
	this.salary = salary;
}
@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
}

}
